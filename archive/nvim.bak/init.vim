" ==============================
" Plugged
" ==============================

call plug#begin(stdpath('data') . '/plugged')
Plug 'chriskempson/base16-vim'
Plug 'danilo-augusto/vim-afterglow'

" Plug 'hrsh7th/nvim-compe'
Plug 'nvim-lua/completion-nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'kabouzeid/nvim-lspinstall'
Plug 'dense-analysis/ale'

Plug 'scrooloose/nerdtree'
Plug 'mbbill/undotree'
Plug 'mileszs/ack.vim'
Plug 'rstacruz/vim-closer'

Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
Plug 'nvim-treesitter/playground'

" telescope dependencies
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
" telescope
Plug 'nvim-telescope/telescope.nvim'

Plug 'dbeniamine/todo.txt-vim'
call plug#end()

" ==============================
" General
" ==============================

colorscheme base16-atelier-savanna
set termguicolors

set listchars=tab:>-,trail:+ " display tabs as '>' and trailing whitespace as '+'
set list

set smarttab
set smartindent     " use the tab style present in the document, which is disabled, i want all spaces all the time
set shiftwidth=4    " number of columns text is indented with reindent operations
set expandtab       " tabs are spaces, this is known


" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile
set undofile
set undodir=~/.config/nvim/undotree

" :W also writes
command! W write

" macros
let @f='%kmfj%jzf`f'

" Open help in vertical tab
augroup vimrc_help
  autocmd!
  autocmd BufEnter *.txt if &buftype == 'help' | wincmd H | endif
augroup END

" Triger `autoread` when files changes on disk
" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
" https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
" Notification after file change
" https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None

" ==============================
" Leader key shortcuts
" ==============================
" keybindings
let mapleader=","       " set the leader key to the comma key
let maplocalleader="-"
" toggle search highlighting
map <leader>h :noh<CR>

map <leader>n :set nonumber!<CR>
map <leader>r :set norelativenumber!<CR>
map <leader>p :set nopaste!<CR>

" Spell checking
" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=
map <leader>w <C-w>w
map <leader>wl <C-w>l
map <leader>wh <C-w>h
map <leader>wj <C-w>j
map <leader>wk <C-w>k
map <leader>wq <C-w>w q
map <leader>x :call ToggleLineCountMarker()<CR>
map <leader>t :NERDTreeToggle<CR>
map <leader>u :UndotreeToggle<CR>
"map <leader>g :GitGutterToggle<CR>
"map <leader>gh :GitGutterLineHighlightsToggle<CR>

" Telescope
" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>


" ==============================
" Plugin Settings
" ==============================

" NERDTree
let NERDTreeQuitOnOpen=1
let NERDTreeIgnore = ['\.pyc$']

" ALE
let g:ale_disable_lsp = 1
let g:ale_fix_on_save = 1
let g:ale_fixers = {
\   'javascript': ['prettier'],
\   'typescript': ['prettier'],
\   'javascriptreact': ['prettier'],
\   'typescriptreact': ['prettier'],
\   'css': ['prettier'],
\}

" nvim-completion

" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect

" Avoid showing message extra message when using completion
set shortmess+=c


" CoC extensions
" let g:coc_global_extensions = ['coc-tsserver']

" " nvim-compe
"
" set completeopt=menuone,noselect
"
" lua <<EOF
" require'compe'.setup {
"   enabled = true;
"   autocomplete = true;
"   debug = false;
"   min_length = 1;
"   preselect = 'enable';
"   throttle_time = 80;
"   source_timeout = 200;
"   resolve_timeout = 800;
"   incomplete_delay = 400;
"   max_abbr_width = 100;
"   max_kind_width = 100;
"   max_menu_width = 100;
"   documentation = true;
"
"   source = {
"     path = true;
"     buffer = true;
"     calc = true;
"     nvim_lsp = true;
"     nvim_lua = true;
"     vsnip = true;
"     ultisnips = true;
"   };
" }
" EOF


" other
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store,*/vendor/*,*/node_modules/*


" ==============================
" Treesitter Settings
" ==============================
lua <<EOF
require "nvim-treesitter.configs".setup {
  highlight = {
    enable = true,
  },
  indent = {
    enable = true
  },
  playground = {
    enable = true,
    disable = {},
    updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
    persist_queries = false, -- Whether the query persists across vim sessions
    keybindings = {
      toggle_query_editor = 'o',
      toggle_hl_groups = 'i',
      toggle_injected_languages = 't',
      toggle_anonymous_nodes = 'a',
      toggle_language_display = 'I',
      focus_language = 'f',
      unfocus_language = 'F',
      update = 'R',
      goto_node = '<cr>',
      show_help = '?',
    },
  },
  javasscript = {
    enable = true,
  },
  typescript = {
    enable = true,
  },
  tsx = {
    enable = true,
  },
  css = {
    enable = true,
  },
  json = {
    enable = true,
  },
}


EOF

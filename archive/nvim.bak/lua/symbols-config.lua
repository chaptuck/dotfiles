vim.g.symbols_outline = {
  highlight_hovered_item = false,
  auto_preview = false,
  symbols_blacklist = {"Variable", "Constant"}
}

local map = vim.api.nvim_set_keymap

vim.g.mapleader=","       -- set the leader key to the comma key
vim.g.maplocalleader="-"

local options = { noremap = true }
-- toggle search highlighting
map('', '<leader>h', ':noh<CR>', {})

map('', '<leader>n', ':set nonumber!<CR>', {})
map('', '<leader>r', ':set norelativenumber!<CR>', {})
map('', '<leader>p', ':set nopaste!<CR>', {})

-- Spell checking
-- Pressing ,ss will toggle and untoggle spell checking
map('', '<leader>ss', ':setlocal spell!<cr>', {})

-- Shortcuts using <leader>
map('', '<leader>sn', ']s', {})
map('', '<leader>sp', '[s', {})
map('', '<leader>sa', 'zg', {})
map('', '<leader>s?', 'z=', {})
map('', '<leader>x', ':call ToggleLineCountMarker()<CR>', {})
-- map('', '<leader>t', ':NERDTreeToggle<CR>', {})
map('', '<leader>t', ':NvimTreeToggle<CR>', {})
map('', '<leader>u', ':UndotreeToggle<CR>', {})
map('', '<leader>gg', ':Gitsigns toggle_current_line_blame<CR>', {})
map('', '<leader>gh', ':Gitsigns toggle_signs<CR>', {})
map('', '<leader>gb', ':GitBlameToggle<CR>', {})
map('', '<leader>a', ':SymbolsOutline<CR>', {})

-- Telescope
-- Find files using Telescope command-line sugar.
map('', '<leader>ff', '<cmd>Telescope find_files<CR>', options)
map('', '<leader>fg', '<cmd>Telescope live_grep<CR>', options)
map('', '<leader>fb', '<cmd>Telescope buffers<CR>', options)
map('', '<leader>fh', '<cmd>Telescope help_tags<CR>', options)

map('', 'gb', ':ls<CR>:b<Space>', options)

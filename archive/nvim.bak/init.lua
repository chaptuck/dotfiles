local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end

require('plugins')

vim.opt.termguicolors = true
vim.cmd('colorscheme base16-tomorrow-night')

require('leader')
require('treesitter')
--require('completion-config')
--require('compe-config')
--require('cmp-config')
--require('lspinstall-config')
require('lspinstaller-config')
require('blame-config')
require('formatter-config')
--require('nvim-autopairs').setup()
require('symbols-config')
require('gitsigns-config')
require('lsp_signature').setup()
require('default_lualine')
require('bufferline-config')
require('nvim-tree-config')
require('illuminate-config')

require('auto-session-config')

vim.opt.listchars="tab:>-,trail:+"
vim.opt.list = true

vim.opt.inccommand="nosplit"
vim.opt.expandtab=true
vim.opt.smarttab=true
vim.opt.smartindent=true
vim.opt.shiftwidth=2
vim.opt.softtabstop=2
vim.opt.mouse="i"
vim.opt.scrolloff=2
vim.opt.timeoutlen=500

vim.opt.ignorecase=true
vim.opt.smartcase=true

-- Allow switching buffers without saving
vim.opt.hidden = true

vim.opt.breakindent = true

vim.opt.backup = false
vim.opt.wb = false
vim.opt.swapfile = false
vim.opt.undofile = true
vim.cmd('set shortmess+=c')

-- :W also writes
vim.cmd('command! W write')
vim.cmd("let @f='%kmfj%jzf`f'")
vim.cmd([[augroup vimrc_help
  autocmd!
  autocmd BufEnter *.txt if &buftype == 'help' | wincmd H | endif
augroup END]])

-- Triger `autoread` when files changes on disk
-- https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
-- https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
vim.cmd("autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif")
-- Notification after file change
-- https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
vim.cmd('autocmd FileChangedShellPost * echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None ')


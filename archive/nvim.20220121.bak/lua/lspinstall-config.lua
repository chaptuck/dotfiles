local util = require 'lspconfig/util'
local attach = function(client)
  require "lsp_signature".on_attach()
  require 'illuminate'.on_attach(client)
end

-- {{{ js/ts

require'lspconfig'.tsserver.setup{
  on_attach = function(client)
    require "lsp_signature".on_attach()
    require 'illuminate'.on_attach(client)
  end;
}

require'lspconfig'.eslint.setup{}

local elixirls_root_path = vim.fn.stdpath('data')..'/lspinstall/elixir'
require'lspconfig'.elixirls.setup{
  cmd = { elixirls_root_path.."/elixir-ls/language_server.sh" };
}

-- }}}


-- {{{ Lua

-- set the path to the sumneko installation; if you previously installed via the now deprecated :LspInstall, use
local sumneko_root_path = vim.fn.stdpath('data')..'/lspinstall/lua'
local sumneko_binary = sumneko_root_path.."/sumneko-lua-language-server"

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

require'lspconfig'.sumneko_lua.setup {
  cmd = {sumneko_binary, "-E", sumneko_root_path .. "/sumneko-lua/extension/server/main.lua"};
  on_attach = attach;

  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
        -- Setup your lua path
        path = runtime_path,
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = {
          'vim',
          'use' -- ignore packer plugin `use`
        },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
}

-- }}}

-- {{{ python

require'lspconfig'.pyright.setup{
  root_dir = util.root_pattern(".git", "pyproject.toml", ".flak8", "requirements.txt");
  --on_attach = attach;
  settings = {
    python = {
      analysis = {
        extraPaths = {
          'sinter'
        },
        typeCheckingMode = 'off'
      }
    }
  };
}

-- }}}

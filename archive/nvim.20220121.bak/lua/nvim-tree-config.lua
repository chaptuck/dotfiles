require 'nvim-tree'.setup({
  auto_close = true,
  git = {
    ignore = true
  },
  filters = {
    custom = {'.git','.hg','.svn','.idea','.DS_Store','vendor','node_modules'}
  }

})
-- Nvim Tree

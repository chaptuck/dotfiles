vim.cmd("autocmd BufEnter * lua require'completion'.on_attach()")

-- Use <Tab> and <S-Tab> to navigate through popup menu

vim.cmd('inoremap <expr> <Tab>   pumvisible() ? "<C-n>" : "<Tab>"')
vim.cmd('inoremap <expr> <S-Tab> pumvisible() ? "<C-p>" : "<S-Tab>"')

require("nvim-treesitter.configs").setup({
	ensure_installed = { "lua", "css", "python", "json", "yaml", "javascript", "typescript", "html" },
	highlight = {
		enable = true,
	},
	indent = {
		enable = true,
	},
})

vim.opt.foldmethod = "expr"

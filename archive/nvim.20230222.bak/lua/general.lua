local set = vim.opt

vim.g.mapleader = " " -- set the leader key to the comma key
vim.g.maplocalleader = "-"
vim.g.python3_host_prog = "/Users/tucker/.pyenv/versions/py3nvim/bin/python"
vim.g.ruby_host_prog = "/Users/tucker/.rbenv/versions/2.7.6/bin/ruby"
vim.g.loaded_perl_provider = 0

vim.cmd([[colorscheme onenord]])

-- Set the behavior of tab
set.tabstop = 2
set.shiftwidth = 2
set.softtabstop = 2
set.expandtab = true

set.number = true
set.ignorecase = true
set.smartcase = true
set.completeopt = "menu,menuone,noselect"

vim.opt.backup = false
vim.opt.wb = false
vim.opt.swapfile = false

-- Allow switching buffers without saving
set.hidden = true

-- Format on save
vim.api.nvim_exec(
	[[
  autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.vue,*.html :EslintFixAll
]],
	true
)

vim.cmd([[augroup vimrc_help
  autocmd!
  autocmd BufEnter *.txt if &buftype == 'help' | wincmd H | endif
augroup END]])

-- Triger `autoread` when files changes on disk
-- https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
-- https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
vim.cmd("autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif")
-- Notification after file change
-- https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
vim.cmd(
	'autocmd FileChangedShellPost * echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None '
)

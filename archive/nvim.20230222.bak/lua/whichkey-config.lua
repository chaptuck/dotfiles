local wk = require("which-key")
-- As an example, we will create the following mappings:
--  * <leader>ff find files
--  * <leader>fr show recent files
--  * <leader>fb Foobar
-- we'll document:
--  * <leader>fn new file
--  * <leader>fe edit file
-- and hide <leader>1

wk.register({
	f = {
		name = "Find", -- optional group name
		f = { "<cmd>Telescope find_files<cr>", "File" },
		r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
		b = { "<cmd>Telescope buffers<CR>", "Buffers" },
		g = { "<cmd>Telescope live_grep<CR>", "Search Files" },
		h = { "<cmd>Telescope help_tags<CR>", "Help Tags" },
		t = { "<cmd>Telescope<cr>", "Telescope All" },
	},
	s = {
		t = { "<cmd>Telescope live_grep<CR>", "Search Text" },
	},
	d = {
		name = "Diagnostics",
		a = { ':lua require("diaglist").open_all_diagnostics()<CR>', "All Diagnostics", noremap = true },
		b = { ':lua require("diaglist").open_buffer_diagnostics()<CR>', "Buffer Diagnostics", noremap = true },
	},
	w = {
		name = "Workspace",
		a = "Add Folder",
		r = "Remove Folder",
		l = "List Folders",
	},
	r = {
		name = "LSP",
		n = "LSP Rename",
	},
	e = { "<cmd>Neotree toggle<CR>", "Neotree" },
}, { prefix = "<leader>" })

wk.register({
	g = {
		name = "GoTo",
	},
})

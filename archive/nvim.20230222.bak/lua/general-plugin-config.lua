local keymap = vim.api.nvim_set_keymap

require("diaglist").init({
	-- optional settings
	-- below are defaults
	debug = false,

	-- increase for noisy servers
	debounce_ms = 150,
})

require("bufferline").setup({
	options = {
		numbers = function(opts)
			return string.format("%s", opts.id)
		end,
	},
})

-- Unless you are still migrating, remove the deprecated commands from v1.x
vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])

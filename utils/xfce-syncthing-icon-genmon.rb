#!/usr/bin/env ruby

require 'httparty'

class Request
  include HTTParty
  base_uri 'http://localhost:8384'
  headers 'X-API-Key' => ARGV[0]
end

class NoApiKeyError < StandardError
  def initialize(msg="NoApiKeyError: Please provide API key as the first argument")
    super
  end
end

class BadApiKeyError < StandardError
  def initialize(msg="BadApiKeyError: Incorrect API key")
    super
  end
end

class SyncthingStatus
  def initialize(folder)
    @folder = folder
    @path = File.dirname(__FILE__)
    @config = Request.get('/rest/system/config')
    @connections = Request.get('/rest/system/connections')
    @status = Request.get("/rest/db/status?folder=#{@folder}")
    @sys_status = Request.get('/rest/system/status')
    @my_id = @sys_status['myID']
    @devices = device_status(devs: @connections['connections'])
    set_device_names(config_devs:@config['devices'], devs:@devices)
  end

  def device_status(devs:)
    devs.each do | id, values |
      if values['connected'] and !values['paused']
        devs[id]['insync'], devs[id]['syncPercent'] = device_sync(id:id, params:values)
      else
        devs[id]['insync'], devs[id]['syncPercent'] = [true, 100]
      end
    end
    devs
  end

  def device_sync(id:, params:)
    sync = Request.get("/rest/db/completion?device=#{id}&folder=#{@folder}")
    [
      sync['completion'] == 100,
      (sync['globalBytes'] - sync['needBytes']) / sync['globalBytes'].to_f * 100
    ]
  end

  def set_device_names(config_devs:, devs:)
    config_devs.each do | cd |
      devs[cd['deviceID']]['name'] = cd['name']
    end
  end

  def get_icon
    icon = "#{@path}/imgs/other.png"
    if @status['state'] == 'syncing'
      icon = "#{@path}/imgs/syncing.png"
    elsif @status['state'] == 'idle'
      icon = "#{@path}/imgs/idle.png"
    end
    @devices.each do | d |
      if !d[1]['insync']
        icon = "#{@path}/imgs/syncing.png"
        break
      end
    end
    icon
  end

  def get_tooltip
    header = [
      "<span size='large' weight='bold'>Syncthing Status</span>",
      ""
    ].join("\n") + "\n"
    body = []
    @devices.each do | key, values |
      if key == @my_id
        tip = "<span weight='bold'>Device:</span> Current \n <span weight='bold'>Status:</span> <span weight='bold' fgcolor='#3299CC'>#{@status['state'].capitalize}</span>"
        percent = @status['localBytes'].to_f / @status['globalBytes'] * 100
        if percent < 100
          tip << "\n Syncing: #{'%.2f' % percent}%"
        end
      else
        if values['connected']
          if (values['insync'])
            status = "<span weight='bold' fgcolor='green'>Up to Date</span>"
          else
            status = "<span weight='bold' fgcolor='red'>Syncing<span>"
          end
        elsif values['paused']
          status = "<span fgcolor='yellow'>Paused</span>"
        else
          status = "<span fgcolor='gray'>Disconnected</span>"
        end
        tip = "<span weight='bold'>Device:</span> #{values['name']} \n <span weight='bold'>Status:</span> #{status}"
        if values['syncPercent'] < 100
          tip << "\n Syncing: #{'%.2f' % values['syncPercent']}%"
        end
      end
      body.push(tip)
    end
    header + body.join("\n")
  end

  def to_s
    [
      "<click>firefox http://localhost:8384</click>",
      "<tool>#{get_tooltip}</tool>",
      "<img>#{get_icon}</img>"
    ].join("\n") + "\n"
  end
end

begin
  if ARGV[0].nil?
    raise NoApiKeyError
  else
    response = Request.get('/rest/system/ping')
    if response.code == 403
      raise BadApiKeyError
    end
  end
  status = SyncthingStatus.new("default")
  puts status
rescue Exception => ex
  puts "<img>#{File.dirname(__FILE__)}/imgs/other.png</img>"
  puts "<tool>#{ex.message}</tool>"
  puts "<click>firefox localhost:8384</click>"
end

#!/usr/bin/env bash

minutes=25
setseconds=false
fabufor=false
slack=false

while getopts "m: s: :l :f :h" option
do
    case "${option}" in
        m)
            minutes=${OPTARG}
            setseconds=false;
            ;;
        s)
            setseconds=true;
            seconds=${OPTARG}
            ;;
        f)
            fabufor=true;
            ;;
        l)
            slack=true;
            ;;
        h)
            helptext=true;
            ;;
    esac
done

if [ "$setseconds" == false ] ; then
    seconds=$(($minutes*60))
fi
date1=$((`date +%s` + $seconds));
half=$(($seconds / 2));

function main() {
    prestart;
    while [ "$date1" -ge `date +%s` ]; do
        timeleft=$(($date1 - `date +%s`))
        output="$(date -u --date @$timeleft +%H:%M:%S)";
        if [ $timeleft -eq $half ]; then
            midway
        fi
        output="$output\r";
        echo -ne $output;
        sleep 1;
    done;
    post;
}

function prestart() {
    if [ "$helptext" = true ]; then
        helptext;
    fi
    if [ "$fabufor" = true ]; then
        fabufor --color "rgb 255 0 0 " --led ALL > /dev/null
    fi
    if [ "$slack" = true ]; then
        slack-cli snooze start --minutes $minutes > /dev/null
    fi
}

function post() {
    notify-send "Countdown Timer" "<i>Time is UP!</i>" -i process-stop;
    if [ "$fabufor" = true ]; then
        fabufor --color "rgb 0 24 255" --led ALL > /dev/null
        fabufor --wave 4 --color "rgb 24 255 0" --repeat 0 --speed 10 > /dev/null
    fi
    ogg123 -qr ~/Sync/alarm.ogg
}

function midway() {
    notify-send "Countdown Timer" "<i>Half way - $output left</i>" -i appointment-soon-symbolic
}

function helptext() {
    echo "Usage: countdown [OPTIONS]"
    echo "Countdown timer. With slack and luxafor connections";
    echo "";
    echo "Countdown Options: ";
    echo "  -l             Snooze slack notifications for duration of countdown"
    echo "  -f             Go red (luxafor required) for duration of countdown"
    echo "  -m [int]       Change duration of timer to [int] minutes (defualt 25 minutes)"
    echo "  -s [int]       Change duration of timer to [int] seconds"
    echo "  -h             Print this help message"
    echo "";
    exit 0;
}

main

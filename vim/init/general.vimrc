set nocompatible
filetype plugin on
set background=dark
syntax enable
set backspace=2 " Backspace behaves like other programs do
set hidden " Undo persists even when switching to different open buffers
set termguicolors
let base16colorspace=256

" colors
colorscheme nord

" general
" set autoread
set lazyredraw
set ttyfast
set exrc " Run project specific .vimrc files
set secure " Disable unsafe commands in project specific vimrc files
set timeoutlen=500 " shorten timeout length for multikey commands (eg <leader>gh)

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile
set undofile
set undodir=~/.vim/undotree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" tabs and spaces
set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " numof of spaces in tab when editing
set smarttab
set smartindent     " use the tab style present in the document, which is disabled, i want all spaces all the time
set shiftwidth=4    " number of columns text is indented with reindent operations
set expandtab       " tabs are spaces, this is known

set listchars=tab:>-,trail:+ " display tabs as '>' and trailing whitespace as '+'
set list

" ui config
set showcmd         " show last command in bottom bar
"set cursorline      " highlight current line
filetype indent on  " load filetype-specific indent files
set path+=**
set wildmenu        " visual autocomplete for command menu
"set showmatch       " highlight matching [{()}]
set so=3            " set X lines to the cursor - when moving vertically with j/k
set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines
"hi Folded ctermfg=3 ctermbg=235
set nofoldenable
" searching
set incsearch       " search as characters are entered
set hlsearch        " hightlight matches
set ignorecase       " be smart about case in search
runtime macros/matchit.vim

" :W also writes
command! W write

" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> /
map <c-space> ?

autocmd BufRead,BufNewFile *.phtml set filetype=php

" macros

let @f='%kmfj%jzf`f'

augroup phpSyntaxOverride
  autocmd!
  autocmd FileType php call PhpSyntaxOverride()
augroup END

" Open help in vertical tab
augroup vimrc_help
  autocmd!
  autocmd BufEnter *.txt if &buftype == 'help' | wincmd H | endif
augroup END

" Triger `autoread` when files changes on disk
" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
" https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
" Notification after file change
" https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None

augroup FiletypeGroup
    autocmd!
    au BufNewFile,BufRead *.jsx set filetype=javascript.jsx
augroup END

augroup FiletypeGroup
    autocmd!
    au BufNewFile,BufRead *.tsx set filetype=typescript.tsx
augroup END


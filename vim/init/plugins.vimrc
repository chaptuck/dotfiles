let NERDTreeQuitOnOpen=1

let g:htl_all_templates=1
let g:html_indent_style1 = "inc"

" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 0
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0

" let g:airline_theme='distinguished'
let g:airline_theme='base16'

"if !exists('g:airline_symbols')
"    let g:airline_symbols = {}
"endif
"
"" unicode and powerline symbols
""let g:airline_left_sep = ''
""let g:airline_left_alt_sep = ''
""let g:airline_right_sep = ''
""let g:airline_rightlt_sep = ''
"let g:airline_symbols.crypt = '🔒'
"let g:airline_symbols.linenr = '☰'
"let g:airline_symbols.maxlinenr = '㏑'
"let g:airline_symbols.branch = '⎇ '
"let g:airline_symbols.paste = 'Þ'
"let g:airline_symbols.spell = 'Ꞩ'
"let g:airline_symbols.notexists = 'Ɇ'
"let g:airline_symbols.whitespace = 'Ξ'
"let g:airline_symbols.dirty=' ☢'

let g:airline#extensions#tabline#enabled = 1 " Enable the list of buffers

let g:monokai_term_italic = 1
let g:monokai_gui_italic = 1

" Sygnify
let g:signify_vcs_list=["git"]

" status line
" set laststatus=2    " Always show the status line

let NERDTreeIgnore = ['\.pyc$']
" Auto open tagbar
" autocmd VimEnter * nested :TagbarOpen

if exists("g:ctrlp_user_command")
    unlet g:ctrlp_user_command
endif

set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store,*/vendor/*,*/node_modules/*

let g:ycm_autoclose_preview_window_after_insertion=1
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_disable_signature_help = 1


highlight  default  link  TodoProject    Keyword
highlight  default  link  TodoPriorityA  String
highlight  default  link  TodoPriorityB  Function
highlight  default  link  TodoPriorityC  Macro

let g:ale_disable_lsp = 1

let g:ackprg = 'ag --vimgrep --smart-case'

" CoC extensions
let g:coc_global_extensions = ['coc-tsserver']

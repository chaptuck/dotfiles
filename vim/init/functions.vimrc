
function! PhpSyntaxOverride()
  hi! def link phpDocTags  phpDefine
  hi! def link phpDocParam phpType
endfunction

let g:matchOn=0
function! ToggleLineCountMarker()
    let l:winview = winsaveview()
    if g:matchOn
        match
        2match
        let g:matchOn = 0
    else
        match ErrorMsg '\%>119v.\+'
        2match DiffAdd '\%<81v.\%>80v'
        let g:matchOn = 1
    en
    call winrestview(l:winview)
endfunction

function! ToggleLineNumber()
  if v:version > 703
    set norelativenumber!
  endif
  set nonumber!
endfunction

function! ToggleThemeMode(style)
    if a:style == "dark"
        set background=dark
        AirlineTheme tomorrow
        colorscheme Tomorrow-Night
    else
        set background=light
        colorscheme Tomorrow
        AirlineTheme tomorrow
    en
endfunction

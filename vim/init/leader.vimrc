" keybindings
let mapleader=" "       " set the leader key to the comma key
let maplocalleader="-"
"  WhichKey setup
nnoremap <silent> <leader>      :<c-u>WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey  ','<CR>

"     toggle search highlighting
map <leader>h :noh<CR>

"     toggle line numbers -- Requires myusuf3/numbers.vim
" nnoremap <leader>n :NumbersToggle<CR>
" nnoremap <leader>r :NumbersOnOff<CR>
"     toggle paste
" nnoremap <leader>n :call ToggleLineNumber()<CR>
map <leader>n :set nonumber!<CR>
map <leader>r :set norelativenumber!<CR>
map <leader>p :set nopaste!<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=
map <leader>w <C-w>w
map <leader>wq <C-w>w q
map <leader>ff :GFiles<cr>
map <leader>fg :Rg<cr>
map <leader>st :Rg<cr>
map <leader>fa :FZF<cr>
map <leader>fb :Buffers<cr>
map <leader>fc :Colors<cr>
map <leader>fl :Lines<cr>
map <leader>fh :History<cr>
map <leader>x :call ToggleLineCountMarker()<CR>
map <leader>t :NERDTreeToggle<CR>
map <leader>u :UndotreeToggle<CR>
"map <leader>g :GitGutterToggle<CR>
"map <leader>gh :GitGutterLineHighlightsToggle<CR>
map <leader>g :SignifyToggle<CR>
map <leader>y :SyntasticToggleMode<CR>
map <leader>ta :TagbarToggle<CR>
map <leader>i :IndentLinesToggle<CR>
map <leader>vd :call ToggleThemeMode('dark')<CR>
map <leader>va :call ToggleThemeMode('')<CR>
nnoremap <silent> <F9> :TagbarToggle<CR>


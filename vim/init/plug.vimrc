call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

Plug 'arcticicestudio/nord-vim'

Plug 'junegunn/vim-peekaboo'
Plug 'unblevable/quick-scope'
Plug 'alvan/vim-closetag'
Plug 'sheerun/vim-polyglot'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'ap/vim-css-color'
Plug 'tommcdo/vim-fugitive-blame-ext'
Plug 'dense-analysis/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'mhinz/vim-signify'
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'mbbill/undotree'
Plug 'editorconfig/editorconfig-vim'
Plug 'mileszs/ack.vim'

Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'jonsmithers/vim-html-template-literals'
Plug 'dbeniamine/todo.txt-vim'
Plug 'liuchengxu/vim-which-key'
call plug#end()

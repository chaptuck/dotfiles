#!/usr/bin/env bash

if [ ! -f $HOME/.vim/autoload/plug.vim ]; then
    echo "Install Vim Plug"
    curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

if [ ! -f $HOME/.vimrc ]; then
    echo "Link .vimrc"
    ln -s $HOME/Code/dotfiles/vim/.vimrc $HOME/.vimrc
fi

if [ ! -d $HOME/.vim/undotree ]; then
    echo "Create undotree directory"
    mkdir -p $HOME/.vim/undotree
fi

if [ ! -d $HOME/.local/bin ]; then
    echo "Create ./local/bin dir"
    mkdir -p $HOME/.local/bin
fi

if [ ! -f $HOME/.local/bin/countdown ]; then
    echo "Link countdown script"
    ln -s $HOME/Code/dotfiles/utils/countdown.sh $HOME/.local/bin/countdown
fi

if [ ! -f $HOME/.git-prompt.sh ]; then
    echo "Link git-prompt.sh"
    ln -s $HOME/Code/dotfiles/.git-prompt.sh $HOME/.git-prompt.sh
fi

if [ ! -f $HOME/.Xmodmap ]; then
    echo "Link .Xmodmap file"
    ln -s $HOME/Code/dotfiles/configs/.Xmodmap $HOME/.Xmodmap
fi

if [ ! -f $HOME/.gitconfig ]; then
    echo "Link .gitconfig file"
    ln -s $HOME/Code/dotfiles/configs/.gitconfig $HOME/.gitconfig
fi

if [ ! -f $HOME/.sqliterc ]; then
    echo "Link .sqliterc file"
    ln -s $HOME/Code/dotfiles/configs/.sqliterc $HOME/.sqliterc
fi

if [ ! -f $HOME/.tmux.conf ]; then
    echo "Link .tmux.conf file"
    ln -s $HOME/Code/dotfiles/configs/.tmux.conf $HOME/.tmux.conf
fi

if [ ! -L $HOME/.vim/init ]; then
    echo "Setup Vim init system"
    mkdir -p $HOME/.vim
    ln -s $HOME/Code/dotfiles/vim/init $HOME/.vim/init
fi

if ! grep -qF "Code/dotfiles/bash/bashrc" $HOME/.bashrc; then
    echo "Source bashrc in .bashrc"
    echo "if [[ -f ~/Code/dotfiles/bash/bashrc ]]; then source ~/Code/dotfiles/bash/bashrc; fi" >> $HOME/.bashrc
fi

if [ ! -f $HOME/.zshrc ]; then
    echo "Link .zshrc"
    ln -s $HOME/Code/dotfiles/zsh/.zshrc $HOME/.zshrc
fi

if [ ! -f $HOME/.oh-my-zsh/custom/themes/chaptuck.zsh-theme ]; then
    echo "Link .zshrc theme"
    ln -s $HOME/Code/dotfiles/zsh/chaptuck.zsh-theme $HOME/.oh-my-zsh/custom/themes/.
fi

git config --global core.editor "vim -u NONE"

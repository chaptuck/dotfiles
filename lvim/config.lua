--[[
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]]
-- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT

-- general
lvim.log.level      = "warn"
lvim.format_on_save = true
lvim.colorscheme    = 'onenord'


-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader                     = "space"
-- add your own keymapping
lvim.keys.normal_mode["<C-s>"]  = ":w<cr>"
lvim.keys.normal_mode["<S-h>"]  = false
lvim.keys.normal_mode["<S-l>"]  = false
lvim.keys.insert_mode["jk"]     = false
lvim.keys.insert_mode["kj"]     = false
lvim.keys.insert_mode["jj"]     = false
lvim.keys.normal_mode["<M-j>"]  = false
lvim.keys.normal_mode["<M-k>"]  = false
vim.opt.scrolloff               = 2
vim.opt.clipboard               = ""
vim.g.gitblame_enabled          = 0
vim.g.gitblame_date_format      = '%r'
vim.g.gitblame_message_template = '<summary> • <date>'

vim.g.minimap_width = 10
vim.g.minimap_auto_start = 1
vim.g.minimap_auto_start_win_enter = 1
-- unmap a default keymapping
-- lvim.keys.normal_mode["<C-Up>"] = false
-- edit a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>"

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
-- local _, actions = pcall(require, "telescope.actions")
-- lvim.builtin.telescope.defaults.mappings = {
--   -- for input mode
--   i = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--     ["<C-n>"] = actions.cycle_history_next,
--     ["<C-p>"] = actions.cycle_history_prev,
--   },
--   -- for normal mode
--   n = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--   },
-- }

-- Use which-key to add extra bindings with the leader-key prefix
-- lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
-- lvim.builtin.which_key.mappings["t"] = {
--   name = "+Trouble",
--   r = { "<cmd>Trouble lsp_references<cr>", "References" },
--   f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
--   d = { "<cmd>Trouble document_diagnostics<cr>", "Diagnostics" },
--   q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
--   l = { "<cmd>Trouble loclist<cr>", "LocationList" },
--   w = { "<cmd>Trouble workspace_diagnostics<cr>", "Wordspace Diagnostics" },
-- }
lvim.builtin.which_key.setup.window.padding = { 1, 1, 1, 1 }
lvim.builtin.which_key.mappings['o'] = { "<cmd>SymbolsOutline<CR>", "Symbols" }
lvim.builtin.which_key.mappings['gt'] = {
  "<cmd>GitBlameToggle<CR>", "Toggle Git Blame"
}
lvim.builtin.which_key.mappings['m'] = {
  name = "Minimap",
  t = {
    "<cmd>MinimapToggle<CR>", "Toggle Minimap"
  },
  c = {
    "<cmd>MinimapRescan<CR>", "Rescan Minimap"
  },
  r = {
    "<cmd>MinimapRefresh<CR>", "Refresh Minimap"
  }
}

lvim.builtin.which_key.mappings['t'] = {
  name = "+Telescope",
  c = { "<cmd>Telescope commands<CR>", "Commands" },
  p = { "<cmd>Telescope projects<CR>", "Projects" },
  b = { "<cmd>Telescope buffers<CR>", "Buffers" },
  r = { "<cmd>Telescope oldfiles<CR>", "Recent Files" },
  s = { "<cmd>Telescope search_history<CR>", "Search History" },
  m = { "<cmd>Telescope marks<CR>", "Marks" },
  l = { "<cmd>Telescope colorscheme<CR>", "Colors" },
  i = { "<cmd>Telescope registers<CR>", "Registers" },
  d = { "<cmd>Telescope diagnostics<CR>", "Diagnostics" },
  t = { "<cmd>Telescope<CR>", "Telescope" },
}

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
-- lvim.builtin.nvimtree.show_icons.git = 0

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "tsx",
  "css",
  "rust",
  "java",
  "yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true

-- generic LSP settings

-- ---@usage disable automatic installation of servers
-- lvim.lsp.automatic_servers_installation = false

-- ---configure a server manually. !!Requires `:LvimCacheReset` to take effect!!
-- ---see the full default list `:lua print(vim.inspect(lvim.lsp.automatic_configuration.skipped_servers))`
-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
local opts = {
  python = {
    analysis = {
      autoSearchPaths = true,
      diagnosticMode = "workspace",
      useLibraryCodeForTypes = true
    }
  },
} -- check the lspconfig documentation for a list of all possible options
require("lvim.lsp.manager").setup("pyright", opts)

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skiipped for the current filetype
-- vim.tbl_map(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- set a formatter, this will override the language server formatting capabilities (if it exists)
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  { command = "black", filetypes = { "python" } },
  -- { command = "isort", filetypes = { "python" } },
  {
    -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
    command = "prettier",
    ---@usage arguments to pass to the formatter
    -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
    -- extra_args = { "--print-with", "100" },
    ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
    filetypes = { "typescript", "typescriptreact" },
  },
}

-- set additional linters
local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  { command = "ruff", filetypes = { "python" } },
  -- {
  --   -- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
  --   command = "shellcheck",
  --   ---@usage arguments to pass to the formatter
  --   -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
  --   extra_args = { "--severity", "warning" },
  -- },
  --   {
  --     command = "codespell",
  --     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
  --     filetypes = { "javascript", "python" },
  --   },
}


lvim.builtin.lualine.sections.lualine_b = {
  {
    'branch',
    "b:gitsigns_head",
    icon = " ",
    color = { gui = "bold" },
    cond = function()
      local window_width_limit = 70
      return vim.fn.winwidth(0) > window_width_limit
    end,
  },

  {
    'filename',
    path = 1,
    symbols = {
      modified = ' [+]',
      readonly = ' [-]',
      unnamed = '[No Name]',
    }
  }
}

-- Additional Plugins
lvim.plugins = {
  { 'shaunsingh/nord.nvim' },
  { 'rmehri01/onenord.nvim',
    config = function()
      require('onenord').setup({
        fade_nc = true,
      })
    end
  },
  { 'sainnhe/edge' },
  { 'hashivim/vim-terraform' },
  { 'jparise/vim-graphql' },
  { 'simrat39/symbols-outline.nvim' },
  { 'f-person/git-blame.nvim' },
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
  { 'wfxr/minimap.vim' },
  --     {"folke/tokyonight.nvim"},
}

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
-- lvim.autocommands.custom_groups = {
--   { "BufWinEnter", "*.lua", "setlocal ts=8 sw=8" },
-- }

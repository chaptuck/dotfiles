local set = vim.opt

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.g.mapleader = " " -- set the leader key to space
vim.g.maplocalleader = "-"
vim.g.loaded_perl_provider = 0

set.termguicolors = true
vim.cmd('colorscheme terafox')

-- Set the behavior of tab
set.tabstop = 2
set.shiftwidth = 2
set.softtabstop = 2
set.expandtab = true
set.scrolloff = 4

set.number = true
set.ignorecase = true
set.smartcase = true
set.completeopt = "menu,menuone,noselect"

set.backup = false
set.wb = false
set.swapfile = false

set.undodir = os.getenv("HOME") .. "/.vim/undodir"
set.undofile = true

-- Allow switching buffers without saving
set.hidden = true

-- Format on save
-- vim.api.nvim_exec(
-- 	[[
--   autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.vue,*.html :EslintFixAll
-- ]],
-- 	true
-- )

vim.cmd([[augroup vimrc_help
  autocmd!
  autocmd BufEnter *.txt if &buftype == 'help' | wincmd H | endif
augroup END]])

-- Triger `autoread` when files changes on disk
-- https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
-- https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
vim.cmd("autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif")
-- Notification after file change
-- https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
vim.cmd(
  'autocmd FileChangedShellPost * echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None '
)

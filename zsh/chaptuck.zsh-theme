## zshmisc https://jlk.fjfi.cvut.cz/arch/manpages/man/zshmisc.1#SIMPLE_PROMPT_ESCAPES
## oh-my-zsh design https://github.com/ohmyzsh/ohmyzsh/wiki/Design

if [[ ( "localhost" =~ $(hostname -s) ) ]]
then
    CHAPTUCK_HOST="%{$fg[green]%}"
    CHAPTUCK_DIR_PATH="%{$fg[blue]%}"
    CHAPTUCK_ARROW="%{$fg[green]%}"
else
    CHAPTUCK_HOST="%{$fg[green]%}"
    CHAPTUCK_DIR_PATH="%{$fg[magenta]%}"
    CHAPTUCK_ARROW="%{$fg[blue]%}"
fi

PROMPT='${CHAPTUCK_HOST}%n%{$fg[cyan]%}@${CHAPTUCK_HOST}%M'
PROMPT+=' ${CHAPTUCK_DIR_PATH}%~%{$reset_color%}
$(git_prompt_info)$(git_prompt_status)$(virtualenv_prompt_info) '
PROMPT+="%(?:${CHAPTUCK_ARROW}➜ :%{$fg[red]%}➜ )%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_DIRTY="%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}✚%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[cyan]%}✹%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}✖%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%}➜%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[cyan]%}═%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[yellow]%}★%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[blue]%}(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$fg[blue]%})%{$reset_color%} "

ZSH_THEME_VIRTUALENV_PREFIX=" %{$fg[blue]%}[%{$fg[yellow]%}"
ZSH_THEME_VIRTUALENV_SUFFIX="%{$fg[blue]%}]%{$reset_color%}"

#!/usr/bin/env python3

import sys

def wait_for_enter():
    print("    ")
    input("Press Enter to continue: ")
    print("    ")

class InstallPackages(object):
    def run(self, context):
        print("Install packages")
        print("    sudo apt install curl vim nodejs npm jq keepassxc vorbis-tools libnotify-bin tldr zsh")
        print("    sudo dnf install curl vim nodejs jq vorbis-tools libnotify tldr zsh")
        wait_for_enter()

class CreateCodeDir(object):
    def run(self, context):
        print("Create Code dir")
        print("    mkdir ~/Code")
        wait_for_enter()

class InstallSlack(object):
    def run(self, context):
        print("Install Slack:")
        print("    https://slack.com/downloads/linux")
        wait_for_enter()
        print("Install slack cli:")
        print("    git clone https://github.com/rockymadden/slack-cli.git ~/Code/slack-cli")
        print("    cd ~/Code/slack-cli")
        print("    make")
        print("    make install")
        print("    slack init")
        print("    (Get legacy token from here https://api.slack.com/custom-integrations/legacy-tokens)")
        wait_for_enter()

class InstallAtom(object):
    def run(self, context):
        print("Install Atom:")
        print("    https://atom.io/")
        wait_for_enter()
        print("Install atom packages:")
        print("    apm stars --install")
        wait_for_enter()

class InstallYarn(object):
    def run(self, context):
        print("Install Yarn:")
        print("    https://yarnpkg.com/en/docs/install")
        wait_for_enter()

class InstallYarnPackages(object):
    def run(self, context):
        print("Set Yarn prefix")
        print("    yarn config set prefix ~/.yarn")
        print("Install Yarn Packages:")
        print("    yarn global add fabufor ascii-themes")
        wait_for_enter()

class InstallZsh(object):
    def run(self, context):
        print("Install Zsh:")
        print("    https://ohmyz.sh/#install")
        print("    ln -s ~/Code/dotfiles/zsh/chaptuck.zsh-theme ~/.oh-my-zsh/custom/themes/.")
        print("    ln -s ~/Code/dotfiles/zsh/.zshrc ~/.zshrc")

if __name__ == "__main__":
    context = {}
    procedure = [
        InstallPackages(),
        CreateCodeDir(),
        InstallSlack(),
        InstallYarn(),
        InstallYarnPackages(),
        InstallZsh(),
    ]
    for step in procedure:
        step.run(context)
    print("Done.")
